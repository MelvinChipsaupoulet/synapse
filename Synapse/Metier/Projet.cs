﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    [Serializable]
    public class Projet
    {
        private string _nom;
        private DateTime _date;
        private DateTime _fin;
        private decimal _prixFactureMo;
        private List<Mission> _missions;
        
        public decimal MargeBruteCourante()
        {
            decimal resultat;
            resultat = this._prixFactureMo - this.CumulCoutMo();
            return resultat;
           
        }

       

        private decimal CumulCoutMo()
        {
            decimal resultat = 0;

            foreach (Mission missionCourante in _missions)
            {
                resultat += missionCourante.Executant.TauxHoraire * missionCourante.NbHeuresEffectuees();
            }

            return resultat;
        }
    }
}
