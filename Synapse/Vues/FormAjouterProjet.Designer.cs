﻿namespace Synapse.Vues
    
{
    partial class FormAjouterProjet 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNom = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.labelDebut = new System.Windows.Forms.Label();
            this.labelFin = new System.Windows.Forms.Label();
            this.labelPrix = new System.Windows.Forms.Label();
            this.textBoxNom = new System.Windows.Forms.TextBox();
            this.textBoxPrix = new System.Windows.Forms.TextBox();
            this.textBoxFin = new System.Windows.Forms.TextBox();
            this.textBoxDebut = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labelNom
            // 
            this.labelNom.AutoSize = true;
            this.labelNom.Location = new System.Drawing.Point(52, 84);
            this.labelNom.Name = "labelNom";
            this.labelNom.Size = new System.Drawing.Size(32, 13);
            this.labelNom.TabIndex = 0;
            this.labelNom.Text = "Nom ";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(301, 289);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 38);
            this.button1.TabIndex = 9;
            this.button1.Text = "VALIDER";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 289);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 38);
            this.button2.TabIndex = 10;
            this.button2.Text = "RETOUR";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // labelDebut
            // 
            this.labelDebut.AutoSize = true;
            this.labelDebut.Location = new System.Drawing.Point(52, 127);
            this.labelDebut.Name = "labelDebut";
            this.labelDebut.Size = new System.Drawing.Size(60, 13);
            this.labelDebut.TabIndex = 11;
            this.labelDebut.Text = "Date début";
            // 
            // labelFin
            // 
            this.labelFin.AutoSize = true;
            this.labelFin.Location = new System.Drawing.Point(52, 170);
            this.labelFin.Name = "labelFin";
            this.labelFin.Size = new System.Drawing.Size(47, 13);
            this.labelFin.TabIndex = 12;
            this.labelFin.Text = "Date Fin";
            // 
            // labelPrix
            // 
            this.labelPrix.AutoSize = true;
            this.labelPrix.Location = new System.Drawing.Point(52, 213);
            this.labelPrix.Name = "labelPrix";
            this.labelPrix.Size = new System.Drawing.Size(56, 13);
            this.labelPrix.TabIndex = 13;
            this.labelPrix.Text = "Prix/heure";
            // 
            // textBoxNom
            // 
            this.textBoxNom.Location = new System.Drawing.Point(143, 84);
            this.textBoxNom.Name = "textBoxNom";
            this.textBoxNom.Size = new System.Drawing.Size(149, 20);
            this.textBoxNom.TabIndex = 14;
            // 
            // textBoxPrix
            // 
            this.textBoxPrix.Location = new System.Drawing.Point(143, 210);
            this.textBoxPrix.Name = "textBoxPrix";
            this.textBoxPrix.Size = new System.Drawing.Size(149, 20);
            this.textBoxPrix.TabIndex = 15;
            // 
            // textBoxFin
            // 
            this.textBoxFin.Location = new System.Drawing.Point(143, 168);
            this.textBoxFin.Name = "textBoxFin";
            this.textBoxFin.Size = new System.Drawing.Size(149, 20);
            this.textBoxFin.TabIndex = 16;
            // 
            // textBoxDebut
            // 
            this.textBoxDebut.Location = new System.Drawing.Point(143, 126);
            this.textBoxDebut.Name = "textBoxDebut";
            this.textBoxDebut.Size = new System.Drawing.Size(149, 20);
            this.textBoxDebut.TabIndex = 17;
            // 
            // FormAjouterProjet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 342);
            this.Controls.Add(this.textBoxDebut);
            this.Controls.Add(this.textBoxFin);
            this.Controls.Add(this.textBoxPrix);
            this.Controls.Add(this.textBoxNom);
            this.Controls.Add(this.labelPrix);
            this.Controls.Add(this.labelFin);
            this.Controls.Add(this.labelDebut);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labelNom);
            this.Name = "FormAjouterProjet";
            this.Text = "FormAjouterProjet";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNom;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label labelDebut;
        private System.Windows.Forms.Label labelFin;
        private System.Windows.Forms.Label labelPrix;
        private System.Windows.Forms.TextBox textBoxNom;
        private System.Windows.Forms.TextBox textBoxPrix;
        private System.Windows.Forms.TextBox textBoxFin;
        private System.Windows.Forms.TextBox textBoxDebut;
    }
}