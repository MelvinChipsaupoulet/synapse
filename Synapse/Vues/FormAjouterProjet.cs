﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Synapse.Utilitaires;
using Synapse.Metier;

namespace Synapse.Vues
{
    public partial class FormAjouterProjet : Form
    {
        public FormAjouterProjet()
        {
            InitializeComponent();
            MinimumSize = Size;
            MaximumSize = Size;
        }

        private bool VerifFormulaireAjoutProjet()
        {
            bool resultat = false;

            if (this.textBoxNom.Text == "")
            {
                MessageBox.Show("Sélectionner le nom du projet");
            }
            else if (this.textBoxDebut.Text == "")
            {
                MessageBox.Show("Saisir la date du début du projet");
            }
            else if (this.textBoxFin.Text == "")
            {
                MessageBox.Show("Saisir la date de la fin du projet");
            }
            else if (this.textBoxPrix.Text == "")
            {
                MessageBox.Show("Saisir le prix de l'heure du projet");
            }
            else
            {
                resultat = true;
            }
            return resultat;
        }


        private void button2_Click(object sender, EventArgs e)
        {
            if (VerifFormulaireAjoutProjet())
            {
               
                string nom = textBoxNom.Text;
                string DateDebut = textBoxDebut.Text;
                string DateFin = textBoxFin.Text;
                string Prix = textBoxPrix.Text;
                Projet unProjet = new Projet(nom, DateDebut, DateFin, Prix);

                unProjet.nom = textBoxNom.Text;
                unProjet.Prix = textBoxPrix.Text;
                if (textBoxDebut.Text != "")
                {
                    unProjet.DateDebut = Convert.ToDateTime(textBoxDebut.Text);
                }
                if (textBoxFin.Text != "")
                {
                    unProjet.DateFin = Convert.ToDateTime(textBoxFin.Text);
                }


            }
                Donnees.CollectionProjet.Add(unProjet);

                ReinitialiserFormulaire();
            }



        }
}
