﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Resources;


namespace Synapse.Vues
{
    public partial class PagePrincipale : Form
    {
        public PagePrincipale()
        {
            InitializeComponent();
        }

        private Form _mdiChild;

        private Form MdiChild

        {
            get { return _mdiChild; }
            set
            {

                if (_mdiChild != null)
                {
                    _mdiChild.Dispose();
                }

                _mdiChild = value;
                _mdiChild.MdiParent = this;
                _mdiChild.MaximumSize = _mdiChild.Size;
                _mdiChild.MinimumSize = _mdiChild.Size;
                _mdiChild.Show();
            }
        }

        private void aJOUTERINTERVENANTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MdiChild = new FormAjouterIntervenant();
        }

        private void aJOUTERMISSIONToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MdiChild = new FormAjouterMission();
        }

        private void aJOUTERPROJETToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormAjouterProjet();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }
    }
}
